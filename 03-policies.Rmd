# Collection & Deposit Policy {#policies}

AAPI Data will be open to both self-deposit by the community and opportunistic collecting by the data curators. This mixed method policy will ensure data sets will be representative of the existing AAPI open datasets, while opening up an opportunity for the community to sustainably contribute to the gaps of evidence that the community needs, as well as does not know they need. 

## Submission Deposit {#deposit}

The data repository will have a user-friendly, front-end online form for community members to upload, describe, and prepare their dataset for ingestion into the AAPI Data repository. In order to prevent barriers to entry the minimum viable package of data will have less required detailed metadata than that which the data curators will collect and contribute. Although, submissions  will be required to include at least 2-3 data contributors and their contact information in order for the data curators to have a main and backup point of contact in order to downstream the data curation process, as needed. See the [Criteria section](https://aapi-data.gitlab.io/data-curation-protocol/policies.html#criteria) for required metadata for data deposits. 

## Opportunistic Collecting {#collecting}

With proper funding and support, AAPI Data repository will have at least one to two data curators who will collect data to be transferred into the repository itself, maintain data integrity and authenticity, as well as fulfill data requests. In opportunistic collecting, data curators will have a more strict criteria to fulfill in order to include an open dataset into the repository.

## Deposit & Collecting Criteria {#criteria}

While all datasets will (eventually) need to be primed to pass as a minimum viable package of open data, fields with an asterisk* are the only fields required for submitters/contributors to have their dataset available in the repository. Data curators will review dataset submissions and prepare for long-term storage and access as necessary.

* **Data file***: This is a digital data file, as well as the structured technical metadata for users and curators to understand to have context before accessing the data file.
  + **Content**: All submissions must be datasets or data files pertaining to AAPI peoples and communities. This can be at the city, county, state, or federal levels.
  + **Size**: This repository will automatically detect the file size. Individual files should not exceed 12 GB, while a collection of files associated with one project deposited into AAPI Data should not exceed 60 GB per year.
  + **Type**: Files can range from tabular, text, still and moving images, and audio. Across cultural and ethnic groups, AAPI communities have historically relied upon oral histories and visual images to retain and disseminate information including family genealogies, mythological stories, culinary recipes, and more. We strive to respectfully capture this valuable information using modern and emerging technologies as they are relevant to the data. 
  + **Formats**: Technical metadata field offering guidelines of the accepted file formats listed below. AAPI Data repository will automatically detect file type, copy, and convert all files to an access and preservation copy (e.g. A deposited WAV will be packaged into multiple MP3 and FLAC files). One copy of these files will be made openly available on the dataset’s public profile page, as well as saved for dark storage preservation.
    + Text
      + Plain text (encoding: USASCII, UTF-8, UTF-16 with BOM)
      + XML (includes XSD/XSL/XHTML, etc.; with included or accessible schema)
      + PDF/A-1 (ISO 19005-1) (*.pdf)
    + Spreadsheet/database
      + Character delimited text (ASCII or Unicode preferred):
      + Comma Separated Values (*.csv)
      + Delimited Text (*.txt)
      + SQL Data Definition Language
    + Audio
      + AIFF (96kHz 16bit PCM) (*.aif, *.aiff)
      + WAV (96kHz 24bit PCM) (*.wav)
    + Imagery
      + TIFF (uncompressed)
      + JPEG2000 (lossless) (*.jp2)
      + PNG (*.png)
    + While these are the preferred file types, AAPI Data is committed to opening up opportunities to help AAPI community members to contribute by dedicating data curator bandwidth to accept requests from contributors to help them to prepare their datasets for submission. 
  + All files will need to be platform-independent, non-proprietary (vendor-independent), no "lossy" or proprietary compression, no embedded files, programs or scripts, no full or partial encryption, and no password protection
* **Contextual information**: These are structured and unstructured descriptive metadata that give context to the dataset.
  + **Description***: A short free-text field that allows the contributor the agency to self-describe the dataset
      + Each dataset’s profile page will also include a front-end crowdsourcing feature where visitors will be able to contribute their metadata perspectives. All posts will be made publicly available, but AAPI Data curators, at their discretion, have the right to hide/omit contributions according to the [Alterations & Removal section](https://aapi-data.gitlab.io/data-curation-protocol/policies.html#alter_remove). 
  + **Geographic scope**: A geospatial map that allows the contributor to pinpoint or draw the parameters in which their dataset covers
  + **Temporal scope***: A date-time range field that follows ISO 8601
  + **Collection method**: A short free-text field for contributors to record the method in which data was gathered (e.g. online survey, phone banking, interviews, etc.)
  + **Author/publishers’ member name and contact information***: Each dataset will be required to have at least two names with contact information, of those who can speak about the data collection method, metadata, and its context to the community. Research and data collection efforts can have high turnover and having multiple related entities will allow data curators more opportunities to fulfill. As this is an effort to boost the AAPI community’s awareness of open datasets, each dataset will need more than one person to be knowledgeable of the dataset’s origin.
* **Administrative***: This is structured metadata that allows contributors to choose their preferred management practices upon the dataset, within AAPI Data repository scope.
  + **License***: A structured metadata field offering contributors to choose a license to their preference. Offered licenses will include:
    + [Public Domain Dedication and License](https://opendatacommons.org/licenses/pddl/index.html) (PDDL) 
    + [Attribution License](https://opendatacommons.org/licenses/by/index.html) (ODC-By)
    + [Open Database License](https://opendatacommons.org/licenses/odbl/index.html) (ODC-ODbL)
    + [Creative Commons](https://creativecommons.org/) licenses for non-dataset files
    + _Other_ will be considered, but thoroughly reviewed by AAPI Data curators.
  + **Agreement to Rights Statement**: Contributors will attest they are not infringing upon anyone's intellectual property rights, privacy rights, or violate any employment or other contractual arrangements
    + Contributors must attest that they have the right to grant the rights as chosen from the license options.
    + Dataset will not contain personally identifying information
  + **Publishing Classification**: In respect to different cultural practices, not all data should be openly and freely available. Contributors will be able to choose from five class levels as adopted from [DataSF’s Data Classification Standard for Sensitive and Protected Datasets](https://datasf.org/publishing/submission-guidelines/) (e.g. public use, internal use, sensitive, protected, and restricted)

## Preservation {#preservation}

After datasets, files, and associated metadata are submitted, data curators will "review all assets, build a study description, enhance documentation, approve the data collection for distribution on the ICPSR website, and archive the data for long-term preservation."^[https://www.icpsr.umich.edu/web/pages/deposit/index.html] Once the data package is archived for preservation, the AAPI Data repository will store and manage the data for a minimum of 12 years following. After 12 years, datasets that are published for internal use, sensitive protected, or restricted will undergo review for further preservation, transfer to new classification, or any other updates.

## Alterations & Removal {#alter_remove}

AAPI Data repository will assign a unique identifier to each data package in order to allow versioning of dataset and file submissions. Versioning will allow contributors and data curators to edit/update the record’s files after they have been published, cite a specific version, and cite all of versions of a data package.^[Adopted from https://help.zenodo.org/#versioning] All contributions are intended to be considered a work in process and are subject to revision and enhancement.

AAPI Data has the right to remove and/or alter a dataset or file immediately if the data is:

* found to be misrepresentative, invading privacy, or that does not reflect evidence of AAPI communities
* redundant to an existing record
* bandwidth in storage and budget cannot support the record

For removal of data, AAPI Data curators' contact information can be found on the homepage.

## Consultation with AAPI Community & Data Curators {#consult}

AAPI Data repository curators are available by appointment to collaborate on data collection, submission, curation, management, and access purposes. AAPI Data repository is committed to integrating AAPI community members into the conversation of creating, maintaining, and disseminating datasets and files. While data curators and community advisors will be available for consultation, on an as needed basis, each record will undergo review by the data curators and community peer reviewers prior to publication and long term preservation.

For consultations on collecting and depositing, AAPI Data curators' contact information can be found on the homepage.

## Curated Datasets {#curated}

After a survey of eleven AAPI-centered datasets, AAPI Data Repository has selected three existing datasets to begin the foundation of the AAPI Data Repository protocol (e.g. collection policy-making and metadata best practices). There are few open datasets serving the diversity of AAPI peoples, and no known repository aggregating these efforts to appropriately represent the growing 7% of AAPI in the United States of Americas.^[ https://www.census.gov/newsroom/facts-for-features/2020/aian.html
]

These three datasets build the blueprint for the AAPI Data Repository, including values from survey participants’ self-description of their identity, in the context of political and community representation (e.g. voting, policy and social attitudes, as well as immigrant background). Each of these attributes can bring deeper context and understanding of the current representation of AAPI communities, in respect of what needs to be done in order to make data contribution, collection, and usage more user-friendly for the community itself.

The foundation for the AAPI Data Repository Protocol is sourced from [three datasets tabluated here](https://gitlab.com/aapi-data/data-curation-protocol/-/blob/master/tables/curated_datasets.md).

The AAPI Data Repository pushes forward the adoption of an intersectional feminist lens to raise the expectation for data collection and analysis to be intentional and embedded within it principles of inclusiveness. In feminism is for everyone, a primer introducing feminist theory to a non-academic audience, cultural critic and intellectual bell hooks writes, “Simply put, feminism is a movement to end sexism, sexist exploitation, and oppression.”^[ bell hooks, Feminism Is for Everybody: Passionate Politics, New York: Routledge (2015), 1.]

Our AAPI Data schema is informed by the three datasets, allowing us to frame our methods and actions taken to align with hook’s vision for translating feminist theory into an inclusive data movement. From these curated datasets, we combined a set of attributes that genuinely represent our target user groups to serve as the repository’s blueprint. Our intention was to select features that might offer a deeper, holistic understanding of AAPI communities and validate or debunk current representations of this group. These attributes include demographic categories: ethnic identity, political leanings, civic engagement, and community affiliations. By making this a responsive and user-friendly process, we hope to encourage active participation in co-creating the AAPI Data Repository from collecting and contributing data to downloading datasets for research and analysis purposes.