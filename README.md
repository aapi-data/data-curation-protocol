## About
This is the AAPI Data Protocol for the (future) AAPI Data Repository, formatted as a book on R Markdown and **bookdown** (https://github.com/rstudio/bookdown). This protocol was written by [143data](https://gitlab.com/143data) & [michi-gato](https://gitlab.com/michi-gato). The full bookdown can be viewed at: https://aapi-data.gitlab.io/data-curation-protocol/

The goals of this data curation protocol addresses a lack of open datasets that are specifically about AAPI experiences. The work of this protocol is inspired by the recently published Data Feminism book, our commitment to the Feminist Data ManifestNo movement, and open data collectives like AAPI Data and Data for Black Lives, as means to integrate accessibility, understanding, and representation of open datasets about Asian American Pacific Islanders (AAPI). 

## More about bookdown
For more information about bookdown, please see the page "[Get Started](https://bookdown.org/yihui/bookdown/get-started.html)" at https://bookdown.org/yihui/bookdown/ for how to compile this example into HTML. You may generate a copy of the book in `bookdown::pdf_book` format by calling `bookdown::render_book('index.Rmd', 'bookdown::pdf_book')`. More detailed instructions are available here https://bookdown.org/yihui/bookdown/build-the-book.html
